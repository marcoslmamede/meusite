FROM php:7.2-apache

RUN apt-get update -q -y \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get install -y -q --no-install-recommends \
        apt-utils \
        vim \
        wget \
        curl \
        nodejs \
        supervisor \
    && apt-get autoclean -y \
	  && apt-get clean -y \
	  && apt-get autoremove -y \
    && npm install node-sass \
    && rm -rf /var/lib/apt/lists/*

COPY docroot/ /var/www/html/sites/
COPY docker/docker-entrypoint.sh /usr/local/bin
COPY docker/supervisor.conf /etc/supervisor/supervisord.conf

WORKDIR /var/www/html

RUN chmod +x /usr/local/bin/docker-entrypoint.sh \
    && chmod -R 755 /var/www/html \
    && chown -R www-data:www-data /var/www/html

ENTRYPOINT ["docker-entrypoint.sh"]