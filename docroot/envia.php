<?php

// Aqui ficam os dados do seu e-mail e da autoresposta!!!

$assunto = "Formulário"; //Assunto do e-mail q vai chegar na sua caixa de mensagem
$mail = "contato@marcosmamede.com.br"; //E-mail que você gostaria de receber os resultados dos formmail's
$assunto_auto = "$nome, Sua Mensagem Foi Recebida Com Sucesso!";//Assunto da Auto Resposta
$website =  "Seu Site";//Nome do Website
$url_website = "www.marcosmamede.com.br";//Url do Website
$nome_webmaster = "Marcos Mamede";//Nome do Webmaster do site
$mensagem_auto = "Obrigado por entrar em contato conosco $nome!\nO mais breve possivel estaremos respondendo sua mensagem!!!\n\nGiStuDios - $nome_webmaster";
$assunto_auto = "Recebemos sua mensagem";

// Aqui ficam os dados do formulário que serão enviados!!!

$nome = $_POST["nome"];//Campo Nome do Formulário
$email = $_POST["email"];//Campo E-mail do Formulário
$assunto_msg = $_POST["assunto_msg"];//Campo Mensagem do Formulário
$contato = $_POST["contato"];//Campo Contato do Formulário
$mensagem = "Formulário enviado por $nome no Website $website:\n\n";//Inicio da Mensagem enviada! 
$mensagem .= "Nome: $nome\n";//Nome do Contato
$mensagem .= "E-mail: $email\n";//Nome do Contato
$mensagem .= "Assunto: $assunto_msg\n";//Assunto do Contato
$mensagem .= "Mensagem: $contato";//Mensagem Enviada do Contato

//não modifique esta linha, pois é ela que envia a mensagem!!!
@mail($mail, $assunto, $mensagem, "From: $email");

//não modifique esta linha, pois é ela que envia a auto_resposta!!!
@mail($email, $assunto_auto, $mensagem_auto, "From: $mail");

?>

<html>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N25TLBJ');</script>
    <!-- End Google Tag Manager -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <!-- Author -->
    <meta name="author" content="Marcos Mamede">
    <!-- description -->
    <meta name="description" content="Marcos Mamede - Gestor de Tráfego Pago (Google Ads, Facebook Ads, Instagram Ads">
    <!-- keywords -->
    <meta name="keywords" content="facebook ads, google ads, instagram ads, marketing digital">
    <!-- Page Title -->
    <title>Marcos Mamede | Gestor de Tráfego | E-mail enviado com sucesso</title>
    <!-- Favicon -->
    <link href="" rel="icon">
    <!-- Bundle -->
    <link href="vendor/css/bundle.min.css" rel="stylesheet">
    <!-- Plugin Css -->
    <link href="creative-startup/css/line-awesome.min.css" rel="stylesheet">
    <link href="vendor/css/revolution-settings.min.css" rel="stylesheet">
    <link href="vendor/css/jquery.fancybox.min.css" rel="stylesheet">
    <link href="vendor/css/owl.carousel.min.css" rel="stylesheet">
    <link href="vendor/css/cubeportfolio.min.css" rel="stylesheet">
    <link href="vendor/css/LineIcons.min.css" rel="stylesheet">
    <link href="creative-startup/css/slick.css" rel="stylesheet">
    <link href="creative-startup/css/slick-theme.css" rel="stylesheet">
    <link href="vendor/css/wow.css" rel="stylesheet">
    <!-- Style Sheet -->
    <link href="creative-startup/css/blog.css" rel="stylesheet">
    <link href="creative-startup/css/style.css" rel="stylesheet">

    <meta property="og:url" content="https://www.marcosmamede.com.br" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Venda mais pela Internet com Google Ads, Facebook Ads, Instagram Ads" />
    <meta property="og:description" content="Através do Marketing Digital você terá estratégias para alcançar seu cliente e fazer com que a Conversão seja seu principal objetivo." />
    <meta property="og:image" content="file:///C:/Users/marco/Desktop/site/megaone.themesindustry.com/creative-startup/img/blog1" />

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N25TLBJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Preloader -->
<div class="preloader">
    <div class="center">
        <div class="loader loader-32">
            <div class='loader-container'>
                <div class='ball-wrapper'>
                    <div class='ball-holder'>
                        <div class='ball'></div>
                    </div>
                    <div class='shadow'></div>
                </div>
                <div class='ball-wrapper'>
                    <div class='ball-holder'>
                        <div class='ball'></div>
                    </div>
                    <div class='shadow'></div>
                </div>
                <div class='ball-wrapper'>
                    <div class='ball-holder'>
                        <div class='ball'></div>
                    </div>
                    <div class='shadow'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Preloader End -->

<!--Header Start-->
<header id="home">

    <div class="inner-header">
        <!--colored-lines-->
        <div class="color-lines row no-gutters">
            <div class="col-4 bg-red"></div>
            <div class="col-4 bg-purple"></div>
            <div class="col-4 bg-green"></div>
        </div>
        <!--upper-nav-->
        <div class="upper-nav">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <ul class="top-personal-info">
                            <!-- <li><a href="#"><i class="las la-phone"></i> +1 631 123 4567</a></li> -->
                            <li><a href="#"><i class="las la-envelope"></i> contato@marcosmamede.com</a></li>
                        </ul>
                    </div>
                    <div class="col-6 text-right">
                        <ul class="top-social-links">
                            <!-- <li><a href="#" class="link-holder fb"><i class="lab la-facebook-f"></i></a></li>
                            <li><a href="#" class="link-holder twit"><i class="lab la-twitter"></i></a></li> -->
                            <li><a href="https://www.youtube.com/channel/UCJMNmpDfr38KeaEEoyC8Ahg?view_as=subscriber" class="link-holder link-in"><i class="lab la-youtube"></i></a></li>
                            <li><a href="https://www.instagram.com/omarcosmamede/" class="link-holder insta"><i class="lab la-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--main nav-->
        <div class="main-navigation">
            <div class="container">
                <div class="row">
                    <div class="col-4 col-lg-3">
                        <a class="navbar-brand simple-nav-logo" href="#home">
                            <img src="creative-startup/img/logo.png" alt="logo">
                        </a>
                        <a class="navbar-brand fixed-nav-logo" href="#home">
                            <img src="creative-startup/img/logo-black.png" alt="logo">
                        </a>
                    </div>
                    <div class="col-8 col-lg-9 simple-navbar">
                        <nav class="navbar navbar-expand-lg">
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active"><a class="nav-link home" href="#home">HOME</a></li>
                                    <li class="nav-item"><a class="nav-link scroll" href="#about-sec">SERVIÇOS</a></li>
                                    <!-- <li class="nav-item"><a class="nav-link scroll" href="#company-portfolio-section">PORTFOLIO</a></li> -->
                                    <li class="nav-item"><a class="nav-link scroll" href="#sobre">SOBRE</a></li>
                                    <!-- <li class="nav-item"><a class="nav-link scroll" href="#blog-sec">BLOG</a></li> -->
                                    <li class="nav-item"><a class="nav-link scroll" href="https://instabio.cc/marcosmamede">CURSOS</a></li>
                                    <li class="nav-item"><a class="nav-link scroll" href="#contact-sec">CONTATO</a></li>
                                </ul>
                            </div>
                        </nav>
                        <ul class="top-social-links fixed-nav-links">
                            <!-- <li><a href="#" class="link-holder fb"><i class="lab la-facebook-f"></i></a></li>
                            <li><a href="#" class="link-holder twit"><i class="lab la-twitter"></i></a></li>
                            <li><a href="#" class="link-holder link-in"><i class="lab la-linkedin-in"></i></a></li> -->
                            <li><a href="https://www.youtube.com/channel/UCJMNmpDfr38KeaEEoyC8Ahg?view_as=subscriber" class="link-holder youtube"><i class="lab la-youtube"></i></a></li>
                            <li><a href="https://www.instagram.com/omarcosmamede/" class="link-holder insta"><i class="lab la-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--toggle btn-->
        <a href="javascript:void(0)" class="sidemenu_btn" id="sidemenu_toggle">
            <span></span>
            <span></span>
            <span></span>
        </a>
    </div>
    <!--Side Nav-->
    <div class="side-menu hidden side-menu-opacity">
        <div class="bg-overlay"></div>
        <div class="inner-wrapper">
            <span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
            <div class="container">
                <div class="row w-100 side-menu-inner-content">
                    <div class="col-12 d-flex justify-content-center align-items-center">
                        <a href="#home" class="navbar-brand"><img src="creative-startup/img/logo.png" alt="logo"></a>
                    </div>
                    <div class="col-12 col-lg-8">
                        <nav class="side-nav w-100">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link scroll" href="#home">HOME</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll" href="#about-sec">SERVIÇOS</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link scroll" href="#company-portfolio-section">PORTFOLIO</a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link scroll" href="#sobre">SOBRE</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll" href="https://instabio.cc/marcosmamede">CURSOS</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link scroll" href="#blog-sec">BLOG</a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link scroll" href="#contact-sec">CONTATO</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-12 col-lg-4 d-flex align-items-center">
                        <div class="side-footer text-white w-100">
                            <div class="menu-company-details">
                                <!-- <span>+1 631 123 4567</span> -->
                                <span>contato@marcosmamede.com.br</span>
                            </div>
                            <ul class="social-icons-simple">
                                <!-- <li><a class="facebook-text-hvr" href="javascript:void(0)"><i class="fab fa-facebook-f"></i> </a> </li>
                                <li><a class="instagram-text-hvr" href="javascript:void(0)"><i class="fab fa-twitter"></i> </a> </li>
                                <li><a class="instagram-text-hvr" href="javascript:void(0)"><i class="fab fa-youtube"></i> </a> </li> -->
                                <li><a class="instagram-text-hvr" href="https://www.youtube.com/channel/UCJMNmpDfr38KeaEEoyC8Ahg?view_as=subscriber"><i class="fab fa-youtube"></i> </a> </li>
                                <li><a class="instagram-text-hvr" href="https://www.instagram.com/omarcosmamede/"><i class="fab fa-instagram"></i> </a> </li>
                            </ul>
                            <p class="text-white">&copy; 2020 Marcos Mamede. Todos os direitos reservados.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="close_side_menu" href="javascript:void(0);"></a>

</header>
<!--Header End-->

<!--Contact section start-->
<section class="contact-sec padding-top padding-bottom" id="contact-sec">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-7">
                <h4 class="heading text-center text-lg-left">Obrigado por entrar em contato, <?php print $nome ?> !!!</h4>
            </div>
            <div class="col-12 col-lg-5 text-center text-lg-left position-relative">
                <div class="contact-details wow fadeInRight">
                    <ul>
                        <li>
                            <span>(19) 98256-6359</span>
                        </li>
                        <li><i class="las la-paper-plane email"></i>contato@marcosmamede.com</li>
                    </ul>
                </div>
                <img src="creative-startup/img/contact-background.png" class="contact-background" alt="contact">
            </div>
        </div>
    </div>
</section>
<!--Contact section end-->

<!--colored-lines-->
<div class="color-lines row no-gutters">
    <div class="col-4 bg-red"></div>
    <div class="col-4 bg-purple"></div>
    <div class="col-4 bg-green"></div>
</div>
<!--Footer Start-->
<footer class="footer-style-1">
    <div class="container">
        <div class="row align-items-center">
            <!--Social-->
            <div class="col-lg-6">
                <div class="footer-social text-center text-lg-left ">
                    <ul class="list-unstyled">
                        <!-- <li><a class="wow fadeInUp" href="javascript:void(0);"><i aria-hidden="true" class="fab fa-facebook-f"></i></a></li> -->
                        <li><a class="wow fadeInDown" href="https://www.youtube.com/channel/UCJMNmpDfr38KeaEEoyC8Ahg?view_as=subscriber"><i aria-hidden="true" class="fab fa-youtube"></i></a></li>
                        <li><a class="wow fadeInUp" href="https://www.instagram.com/omarcosmamede/"><i aria-hidden="true" class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <!--Text-->
            <div class="col-lg-6 text-center text-lg-right">
                <p class="company-about fadeIn">© 2020 Marcos Mamede. Todos os direitos reservados.
                </p>
            </div>
        </div>
    </div>
</footer>
<!--Footer End-->

<!--Scroll Top Start-->
<span class="scroll-top-arrow"><i class="fas fa-angle-up"></i></span>
<!--Scroll Top End-->

<!-- JavaScript -->
<script src="vendor/js/bundle.min.js"></script>
<!-- Plugin Js -->
<script src="vendor/js/jquery.appear.js"></script>
<script src="vendor/js/jquery.fancybox.min.js"></script>
<script src="vendor/js/owl.carousel.min.js"></script>
<script src="vendor/js/parallaxie.min.js"></script>
<script src="vendor/js/wow.min.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="vendor/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/js/jquery.themepunch.revolution.min.js"></script>
<script src="vendor/js/jquery.cubeportfolio.min.js"></script>
<!-- SLIDER REVOLUTION EXTENSIONS -->
<script src="vendor/js/extensions/revolution.extension.actions.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.migration.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="vendor/js/extensions/revolution.extension.video.min.js"></script>
<!-- google map-->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJRG4KqGVNvAPY4UcVDLcLNXMXk2ktNfY"></script>-->
<!--<script src="creative-startup/js/map.js"></script>-->
<!--Tilt Js-->
<script src="creative-startup/js/slick.js"></script>
<script src="creative-startup/js/slick.min.js"></script>
<!-- custom script-->
<script src="creative-startup/js/circle-progress.min.js"></script>

<script src="creative-startup/js/script.js"></script>

</body>
</html>
